package org.cern.nile.configs;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Properties;
import org.cern.nile.configs.StreamConfig.ClientProperties;
import org.cern.nile.configs.StreamConfig.CommonProperties;
import org.cern.nile.configs.StreamConfig.DecodingProperties;
import org.junit.jupiter.api.Test;

class PropertiesCheckTest {

  @Test
  void validateProperties_ThrowsRuntimeException_forIllegalArguments() {
    assertThrows(RuntimeException.class,
        () -> PropertiesCheck.validateProperties(null),
        "Properties object cannot be null");

  }

  @Test
  void validateProperties_PassesValidation_forDecoding() {
    final Properties properties = new Properties();
    initClientAndCommonProperties(properties);
    properties.put(DecodingProperties.SINK_TOPIC.getValue(), "");
    PropertiesCheck.validateProperties(properties);
  }

  private void initClientAndCommonProperties(Properties properties) {
    properties.put(ClientProperties.CLIENT_ID.getValue(), "");
    properties.put(ClientProperties.KAFKA_CLUSTER.getValue(), "");
    properties.put(ClientProperties.SOURCE_TOPIC.getValue(), "");
    properties.put(ClientProperties.TRUSTSTORE_LOCATION.getValue(), "");
    properties.put(CommonProperties.STREAM_CLASS.getValue(), "");
  }
}
