package org.cern.nile.serdes.otlp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import io.opentelemetry.proto.logs.v1.LogsData;
import java.io.IOException;
import org.cern.nile.OtlpHelper;
import org.cern.nile.exceptions.OtlpProcessingException;
import org.junit.jupiter.api.Test;


public class OtlpLogsSerdeTest {

  @Test
  public void deserialize_DeserializedData_WhenRecordsCorrectlySerialized() throws IOException {
    byte[] serializedData;
    LogsData originalData;
    LogsData deserializedData;
    try (OtlpLogsSerde serde = new OtlpLogsSerde()) {
      originalData = OtlpHelper.getDummyLogsData();
      serializedData = serde.serializer().serialize("", originalData);
      deserializedData = serde.deserializer().deserialize("", serializedData);
    }
    assertNotNull(deserializedData);
    assertEquals(originalData, deserializedData);
  }

  @Test
  public void serialize_ReturnsNull_WhenDataIsNull() {
    try (OtlpLogsSerde serde = new OtlpLogsSerde()) {
      byte[] serializedData = serde.serializer().serialize("", null);
      assertNull(serializedData);
    }
  }

  @Test
  public void deserialize_ThrowsException_WhenDataCorrupted() {
    byte[] corruptedData = "corruptedData".getBytes();

    try (OtlpLogsSerde serde = new OtlpLogsSerde()) {
      assertThrows(OtlpProcessingException.class, () -> serde.deserializer().deserialize("", corruptedData));
    }
  }

}