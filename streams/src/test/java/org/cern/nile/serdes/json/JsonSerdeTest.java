package org.cern.nile.serdes.json;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;

public class JsonSerdeTest {

  @Test
  public void testConfigure() {
    try (JsonSerde jsonSerde = new JsonSerde()) {
      Map<String, Object> configs = new HashMap<>();
      configs.put("config-key", "config-value");
      jsonSerde.configure(configs, true);
      assertNotNull(jsonSerde.serializer());
      assertNotNull(jsonSerde.deserializer());
    }
  }

}
