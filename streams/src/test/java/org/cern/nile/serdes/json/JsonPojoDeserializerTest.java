package org.cern.nile.serdes.json;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Map;
import org.cern.nile.models.Application;
import org.cern.nile.models.Topic;
import org.junit.jupiter.api.Test;

class JsonPojoDeserializerTest {

  private final JsonPojoDeserializer<Application> applicationDeserializer = new JsonPojoDeserializer<>(Application.class);
  private final JsonPojoDeserializer<Topic> topicDeserializer = new JsonPojoDeserializer<>(Topic.class);

  @Test
  void deserialize_Application_ReturnsApplication() {
    String json = "{\"name\":\"my-app\",\"topic\":{\"name\":\"my-topic\"}}";
    Application expected = new Application();
    expected.setName("my-app");
    expected.setTopic(new Topic());
    expected.getTopic().setName("my-topic");
    Application actual = applicationDeserializer.deserialize("test-topic", json.getBytes());
    assertEquals(expected.toString(), actual.toString());
  }

  @Test
  void deserialize_Topic_ReturnsTopic() {
    String json = "{\"name\":\"my-topic\"}";
    Topic expected = new Topic();
    expected.setName("my-topic");
    Topic actual = topicDeserializer.deserialize("test-topic", json.getBytes());
    assertEquals(expected.toString(), actual.toString());
  }

  @Test
  void deserialize_NullBytes_ReturnsNull() {
    assertNull(applicationDeserializer.deserialize("test-topic", null));
  }

  @Test
  void deserialize_NullJson_ReturnsNull() {
    assertNull(applicationDeserializer.deserialize("test-topic", "null".getBytes()));
  }

  @Test
  void configure_SetJsonPOJOClass_SetsClass() {
    try (JsonPojoDeserializer<Topic> deserializer = new JsonPojoDeserializer<>()) {
      assertNull(deserializer.tClass);
      deserializer.configure(Map.of("JsonPOJOClass", Topic.class), true);
      assertEquals(Topic.class, deserializer.tClass);
    }
  }

}
