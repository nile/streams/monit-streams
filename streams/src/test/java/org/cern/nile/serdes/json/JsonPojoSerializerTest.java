package org.cern.nile.serdes.json;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;

class JsonPojoSerializerTest {

  @Test
  void configure_doesNotThrowException() {
    try (JsonPojoSerializer<Object> serializer = new JsonPojoSerializer<>()) {
      serializer.configure(Collections.emptyMap(), true);
    }
  }

  @Test
  void serialize_withNullData_ReturnsNull() {
    try (JsonPojoSerializer<Object> serializer = new JsonPojoSerializer<>()) {
      assertNull(serializer.serialize("topic", null));
    }
  }

  @Test
  void serialize_withNonNullData_ReturnsJsonBytes() {
    Map<String, String> data = new HashMap<>();
    data.put("key", "value");

    byte[] expectedBytes = "{\"key\":\"value\"}".getBytes();

    try (JsonPojoSerializer<Map<String, String>> serializer = new JsonPojoSerializer<>()) {
      assertArrayEquals(expectedBytes, serializer.serialize("topic", data));
    }
  }

  @Test
  void close_doesNotThrowException() {
    JsonPojoSerializer<Object> serializer = new JsonPojoSerializer<>();
    serializer.close();
  }

}
