package org.cern.nile.streams;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.google.gson.JsonObject;
import io.opentelemetry.proto.logs.v1.LogsData;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.streams.TestOutputTopic;
import org.apache.kafka.streams.test.TestRecord;
import org.cern.nile.OtlpHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class OtlpLogsDataDecodeTest extends OtlpBaseStreamDecodeTest {
  TestOutputTopic<String, JsonObject> testOutputTopic;

  @Override
  protected AbstractStream createStreamDecodeInstance() {
    return new OtlpLogsDataDecode(OtlpBaseStreamDecodeTest.SOURCE_TOPIC, OtlpBaseStreamDecodeTest.SINK_TOPIC);
  }

  @BeforeEach
  void setUp() throws IOException {
    super.setUp();
    testOutputTopic = testDriver.createOutputTopic(SINK_TOPIC, KEY_DESERIALIZER, VALUE_DESERIALIZER);
  }

  @Test
  public void otlpLogDecode_DecodesLogsDataToJson_ForCorrectInput() throws IOException {
    LogsData logsData = OtlpHelper.getDummyLogsData();
    pipeRecord(logsData);

    int count = 0;
    final int expectedRecords = 7;

    Map<String, Object> commonAttributes = new HashMap<>();
    commonAttributes.put("toplevel_hostgroup", "monitoring");
    commonAttributes.put("argv", "/usr/sbin/fluent-bit -c /etc/fluent-bit/monit-agent/fluent-bit.conf");
    commonAttributes.put("log_type", "netlog");
    commonAttributes.put("environment", "monit3592");
    commonAttributes.put("file", "/usr/sbin/fluent-bit");
    commonAttributes.put("tty", "(none)");
    commonAttributes.put("hostgroup", "monitoring/spare");
    commonAttributes.put("action", "connect");

    while (count < expectedRecords) {
      final TestRecord<String, JsonObject> outputRecord = testOutputTopic.readRecord();

      assertNotNull(outputRecord);
      JsonObject recordValue = outputRecord.value();

      for (Map.Entry<String, Object> entry : commonAttributes.entrySet()) {
        assertEquals(entry.getValue(), recordValue.get(entry.getKey()).getAsString());
      }

      assertTrue(recordValue.has("@timestamp"));
      assertTrue(recordValue.has("timestamp_unix_nano"));

      count++;
    }

    assertEquals(expectedRecords, count);
  }

}
