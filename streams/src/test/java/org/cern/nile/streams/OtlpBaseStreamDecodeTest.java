package org.cern.nile.streams;

import com.google.gson.JsonObject;
import io.opentelemetry.proto.logs.v1.LogsData;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

import lombok.extern.java.Log;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.test.TestRecord;
import org.cern.nile.configs.StreamConfig;
import org.cern.nile.serdes.json.JsonSerde;
import org.cern.nile.serdes.otlp.OtlpLogsSerde;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class OtlpBaseStreamDecodeTest {
  protected static final Logger LOGGER = LoggerFactory.getLogger(OtlpBaseStreamDecodeTest.class.getName());
  protected static final String ROUTING_PROPERTIES = "src/test/resources/routing.properties";

  protected static final Serializer<String> KEY_SERIALIZER = Serdes.String().serializer();
  protected static final Serializer<LogsData> VALUE_SERIALIZER = new OtlpLogsSerde().serializer();
  protected static final Deserializer<String> KEY_DESERIALIZER = Serdes.String().deserializer();
  protected static final Deserializer<JsonObject> VALUE_DESERIALIZER = new JsonSerde().deserializer();

  protected static final String SOURCE_TOPIC = StreamConfig.ClientProperties.SOURCE_TOPIC.getValue();
  protected static final String SINK_TOPIC = StreamConfig.DecodingProperties.SINK_TOPIC.getValue();

  protected TopologyTestDriver testDriver;

  protected abstract AbstractStream createStreamDecodeInstance();

  @BeforeEach
  void setUp() throws IOException {
    final StreamsBuilder builder = new StreamsBuilder();

    final Properties routingProperties = new Properties();
    routingProperties.load(new FileInputStream(ROUTING_PROPERTIES));

    final AbstractStream streamDecode = createStreamDecodeInstance();
    streamDecode.configure(routingProperties);
    streamDecode.createTopology(builder);

    Topology topology = builder.build();

    LOGGER.debug("Created topology: {}\n", topology.describe());

    Properties testDriverProperties = new Properties();
    testDriverProperties.setProperty(StreamsConfig.APPLICATION_ID_CONFIG, "test-app");
    testDriverProperties.setProperty(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9091");
    testDriverProperties.setProperty(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
    testDriverProperties.setProperty(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, OtlpLogsSerde.class.getName());
    testDriverProperties.setProperty(StreamsConfig.STATE_DIR_CONFIG, "target/kafka-streams");
    testDriver = new TopologyTestDriver(topology, testDriverProperties);
  }

  @AfterEach
  void tearDown() throws IOException {
    try {
      testDriver.close();
    } catch (Exception e) {
      Files.deleteIfExists(Path.of("target/kafka-streams"));
      LOGGER.error("Error closing test driver", e);
    }
  }

  protected void pipeRecord(LogsData record) {
    //final TestRecordRecord<byte[], byte[]> inputRecord =
    TestInputTopic<String, LogsData> testInputTopic;
    testInputTopic = testDriver.createInputTopic(SOURCE_TOPIC, KEY_SERIALIZER, VALUE_SERIALIZER);
    testInputTopic.pipeInput(record);
  }

}
