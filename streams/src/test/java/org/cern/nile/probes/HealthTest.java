package org.cern.nile.probes;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KafkaStreams.State;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class HealthTest {

  private static final String HEALTH_SERVER_URL = "http://localhost:8899/health";

  @Mock
  private KafkaStreams mockStreams;

  private Health health;

  @BeforeEach
  void setUp() {
    health = new Health(mockStreams);
  }

  @Test
  void start_Returns200_forNotRunningStream() throws IOException {
    when(mockStreams.state()).thenReturn(State.RUNNING);
    health.start();
    URL url = new URL(HEALTH_SERVER_URL);
    HttpURLConnection conn = (HttpURLConnection) url.openConnection();

    assertEquals(200, conn.getResponseCode());
    health.stop();
  }

  @Test
  void start_Returns500_forNotRunningStream() throws IOException {
    when(mockStreams.state()).thenReturn(State.NOT_RUNNING);
    health.start();
    URL url = new URL(HEALTH_SERVER_URL);
    HttpURLConnection conn = (HttpURLConnection) url.openConnection();

    assertEquals(500, conn.getResponseCode());
    health.stop();
  }

  @Test
  void stop_StopsTheServer() throws IOException {
    health.start();
    health.stop();
    URL url = new URL(HEALTH_SERVER_URL);
    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    assertThrows(ConnectException.class, conn::getResponseCode);
  }

}
