package org.cern.nile;

import com.google.protobuf.InvalidProtocolBufferException;
import io.opentelemetry.proto.logs.v1.LogsData;
import java.io.IOException;
import java.io.InputStream;

public class OtlpHelper {
  protected static final String LOGS_DATA_PROTO_BYTES = "logsDataProtoBytes";

  public static LogsData getDummyLogsData() throws IOException {
    try (InputStream logsDataStream = OtlpHelper.class.getClassLoader().getResourceAsStream(LOGS_DATA_PROTO_BYTES)) {
      assert logsDataStream != null;
      byte[] dataBytes = logsDataStream.readAllBytes();
      return bytesToLogsData(dataBytes);
    }
  }

  private static LogsData bytesToLogsData(byte[] dataBytes) throws InvalidProtocolBufferException {
    return LogsData.parseFrom(dataBytes);
  }

}
