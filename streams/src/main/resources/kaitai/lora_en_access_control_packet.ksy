meta:
  id: lora_en_access_control_packet
  endian: be
  bit-endian: be
  title: Lora EN access control packet

doc-ref: https://adeunis.notion.site/Technical-Reference-Manual-DRY-CONTACTS-LoRaWAN-Sigfox-c4540f166e484a269b4fcb79c090a587

seq:
  - id: frame_code
    type: u1
    doc: Frame code
  - id: payload
    type:
      switch-on: frame_code
      cases:
        64: data_frame
        48: keep_alive_frame
        _: unsupported_frame

types:
  status_byte_bits:
    seq:
      - id: frame_counter
        type: b3
      - id: app_flag_2_hidden
        type: b1
      - id: app_flag_1_hidden
        type: b1
      - id: timestamp_status_hidden
        type: b1
      - id: low_bat
        type: b1
      - id: config
        type: b1

  data_channel_states_bits:
    seq:
      - id: channel_4_previous_state
        type: b1
      - id: channel_4_current_state
        type: b1
      - id: channel_3_previous_state
        type: b1
      - id: channel_3_current_state
        type: b1
      - id: channel_2_previous_state
        type: b1
      - id: channel_2_current_state
        type: b1
      - id: channel_1_previous_state
        type: b1
      - id: channel_1_current_state
        type: b1

  keep_alive_channel_states_bits:
    seq:
      - id: app_flag_10_hidden
        type: b1
      - id: app_flag_11_hidden
        type: b1
      - id: app_flag_12_hidden
        type: b1
      - id: app_flag_13_hidden
        type: b1
      - id: channel_4_current_state
        type: b1
      - id: channel_3_current_state
        type: b1
      - id: channel_2_current_state
        type: b1
      - id: channel_1_current_state
        type: b1

  data_frame:
    seq:
      - id: status_byte
        type: status_byte_bits
        doc: Status byte
      - id: channel_1_info
        type: u2
        doc: Channel 1 info (event counter if configured in input mode, current output state if configured in output mode)
      - id: channel_2_info
        type: u2
        doc: Channel 2 info (event counter if configured in input mode, current output state if configured in output mode)
      - id: channel_3_info
        type: u2
        doc: Channel 3 info (event counter if configured in input mode, current output state if configured in output mode)
      - id: channel_4_info
        type: u2
        doc: Channel 4 info (event counter if configured in input mode, current output state if configured in output mode)
      - id: channel_states
        type: data_channel_states_bits
        doc: Data channel states (define precisely the input/output state)

  keep_alive_frame:
    seq:
      - id: status_byte
        type: status_byte_bits
        doc: Status byte
      - id: channel_1_info
        type: u2
        doc: Channel 1 info (global event counter if configured in input mode)
      - id: channel_2_info
        type: u2
        doc: Channel 2 info (global event counter if configured in input mode)
      - id: channel_3_info
        type: u2
        doc: Channel 3 info (global event counter if configured in input mode)
      - id: channel_4_info
        type: u2
        doc: Channel 4 info (global event counter if configured in input mode)
      - id: channel_states
        type: keep_alive_channel_states_bits
        doc: Keep Alive channel states (define precisely the input/output state)

  unsupported_frame:
    seq:
      - id: unsupported_data
        size-eos: true
        doc: Unsupported data payload
