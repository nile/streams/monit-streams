meta:
  id: lora_rising_hf_packet
  endian: le
  bit-endian: le
  title: BE IT's Rising HF packet

doc-ref: https://risinghf.com/wp-content/uploads/2020/07/RHF76-052_Rev1.3.pdf

seq:
  - id: header
    type: u1
    doc: Frame magic byte.
  - id: temperature_raw_hidden
    type: u2
    doc: Raw temperature reading from the sensor. The actual value can be calculated as (175.72 * temperature_raw) / 65536.0 - 46.85.
  - id: humidity_raw_hidden
    type: u1
    doc: Raw humidity reading from the sensor. The actual value can be calculated as (125 * humidity_raw) / 256.0 - 6.
  - id: period_raw_hidden
    type: u2
    doc: Raw transmission period setting from the sensor, in seconds. The actual value can be calculated as period_raw * 2.0.
  - id: rssi_raw_hidden
    type: u1
    doc: Raw received signal strength indication (RSSI), in dBm. The actual value can be calculated as -180 + rssi_raw.
  - id: snr_raw_hidden
    type: s1
    doc: Raw signal-to-noise ratio. The actual value can be calculated as snr_raw / 4.0.
  - id: battery_raw_hidden
    type: u1
    doc: Raw battery voltage reading. The actual value can be calculated as (battery_raw + 150) * 0.01.

instances:
  temperature:
    value: (175.72 * temperature_raw_hidden) / 65536.0 - 46.85
    doc: Temperature reading in degrees Celsius.
  humidity:
    value: (125 * humidity_raw_hidden) / 256.0 - 6
    doc: Humidity reading in percent.
  period:
    value: period_raw_hidden * 2.0
    doc: Transmission period in seconds.
  rssi:
    value: -180.0 + rssi_raw_hidden
    doc: Received signal strength indication (RSSI) in dBm.
  snr:
    value: snr_raw_hidden / 4.0
    doc: Signal-to-noise ratio (SNR) in dB.
  battery:
    value: (battery_raw_hidden + 150) * 0.01
    doc: Battery voltage in volts.
