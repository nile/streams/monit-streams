meta:
  id: lora_batmon_packet
  endian: le
  bit-endian: le
  title: BE Batmon's packet

doc-ref: https://cernbox.cern.ch/index.php/s/sD0QRvGuEOWaxnQ

seq:
  - id: packet_number
    type: u2
    doc: Number of the packet.
  - id: swbuild
    type: u1
    doc: Dummy Byte. Set to 0xFF (255).
  - id: dummy_byte
    type: u1
    doc: Dummy Byte. Set to 0xFF (255).
  - id: status_flag
    type: u2
    doc: The status flag. Always set to 0x0000 (0).
  - id: mon_3_3_raw_hidden
    type: u2
    doc: Raw reading by the ADC of the output of the Voltage Regulator which supplies 3.3V to the BatMon. The actual value can be calculated as mon_3_3_raw * 6.6 / 4096.
  - id: mon_5_raw_hidden
    type: u2
    doc: Raw reading by the ADC of the output of the Voltage Regulator which supplies 5V to the BatMon. The actual value can be calculated as mon_5_raw * 6.6 / 4096.
  - id: v_bat_raw_hidden
    type: u2
    doc: Raw reading by the ADC of the Voltage supplied by the Batteries. The actual value can be calculated as v_bat_raw * 3.3 / 0.2174 / 4096.
  - id: extwtd_cnt
    type: u2
    doc: Incremental counter of the External Watchdog.
  - id: fgdos1_sensor_frequency_7_0_hidden
    type: u1
    doc: The current frequency of the FGDOS1 sensor (bits 23 down to 16).
  - id: fgdos1_sensor_frequency_15_8_hidden
    type: u1
    doc: The current frequency of the FGDOS1 sensor (bits 15 down to 8).
  - id: fgdos1_sensor_frequency_23_16_hidden
    type: u1
    doc: The current frequency of the FGDOS1 sensor (bits 7 down to 0).
  - id: fgdos1_target_frequency_7_0_hidden
    type: u1
    doc: The frequency provided by a reference oscillator of FGDOS1. It is not changed with dose but with temperature so it can be used to compensate the for the latter (bits 23 down to 16).
  - id: fgdos1_target_frequency_15_8_hidden
    type: u1
    doc: The frequency provided by a reference oscillator of FGDOS1. It is not changed with dose but with temperature so it can be used to compensate the for the latter (bits 15 down to 8).
  - id: fgdos1_target_frequency_23_16_hidden
    type: u1
    doc: The frequency provided by a reference oscillator of FGDOS1. It is not changed with dose but with temperature so it can be used to compensate the for the latter (bits 7 down to 0).
  - id: fgdos1_recharge_count
    type: u1
    doc: Number of recharge done by FGDOS1.
  - id: fgdos1_recharge_frequency_7_0_hidden
    type: u1
    doc: The frequency reached below the threshold, at which the previous recharge was done (<49152kHz) (bits 23 down to 16).
  - id: fgdos1_recharge_frequency_15_8_hidden
    type: u1
    doc: The frequency reached below the threshold, at which the previous recharge was done (<49152kHz) (bits 15 down to 8).
  - id: fgdos1_recharge_frequency_23_16_hidden
    type: u1
    doc: The frequency reached below the threshold, at which the previous recharge was done (<49152kHz) (bits 7 down to 0).
  - id: fgdos2_sensor_frequency_7_0_hidden
    type: u1
    doc: The current frequency of the FGDOS2 sensor (bits 23 down to 16).
  - id: fgdos2_sensor_frequency_15_8_hidden
    type: u1
    doc: The current frequency of the FGDOS2 sensor (bits 15 down to 8).
  - id: fgdos2_sensor_frequency_23_16_hidden
    type: u1
    doc: The current frequency of the FGDOS2 sensor (bits 7 down to 0).
  - id: fgdos2_target_frequency_7_0_hidden
    type: u1
    doc: The frequency provided by a reference oscillator of FGDOS2. It is not changed with dose but with temperature so it can be used to compensate the for the latter (bits 23 down to 16).
  - id: fgdos2_target_frequency_15_8_hidden
    type: u1
    doc: The frequency provided by a reference oscillator of FGDOS2. It is not changed with dose but with temperature so it can be used to compensate the for the latter (bits 15 down to 8).
  - id: fgdos2_target_frequency_23_16_hidden
    type: u1
    doc: The frequency provided by a reference oscillator of FGDOS2. It is not changed with dose but with temperature so it can be used to compensate the for the latter (bits 7 down to 0).
  - id: fgdos2_recharge_count
    type: u1
    doc: Number of recharge done by FGDOS2
  - id: fgdos2_recharge_frequency_7_0_hidden
    type: u1
    doc: The frequency reached below the threshold, at which the previous recharge was done (<49152kHz) (bits 23 down to 16).
  - id: fgdos2_recharge_frequency_15_8_hidden
    type: u1
    doc: The frequency reached below the threshold, at which the previous recharge was done (<49152kHz) (bits 15 down to 8).
  - id: fgdos2_recharge_frequency_23_16_hidden
    type: u1
    doc: The frequency reached below the threshold, at which the previous recharge was done (<49152kHz) (bits 7 down to 0).
  - id: toshiba_seu_m1
    type: u2
    doc: The total number of SEUs measured by the TOSHIBA sensor during the measurement period (M1).
  - id: toshiba_seu_m2
    type: u2
    doc: The total number of SEUs measured by the TOSHIBA sensor during the measurement period (M2).
  - id: toshiba_seu_m3
    type: u2
    doc: The total number of SEUs measured by the TOSHIBA sensor during the measurement period (M3).
  - id: toshiba_seu_m4
    type: u2
    doc: The total number of SEUs measured by the TOSHIBA sensor during the measurement period (M4).
  - id: cypress_seu_m5
    type: u2
    doc: The total number of SEUs measured by the Cypress Memory during the measurement period (M5).
  - id: cypress_seu_m6
    type: u2
    doc: The total number of SEUs measured by the Cypress Memory during the measurement period (M6).
  - id: cypress_seu_m7
    type: u2
    doc: The total number of SEUs measured by the Cypress Memory during the measurement period (M7).
  - id: cypress_seu_m8
    type: u2
    doc: The total number of SEUs measured by the Cypress Memory during the measurement period (M8).

instances:
  mon_3_3:
    value: mon_3_3_raw_hidden  * 6.6 / 4096
    doc: 3.3V monitor in mV.
  mon_5:
    value: mon_5_raw_hidden  * 6.6 / 4096
    doc: 5V monitor in mV.
  v_bat:
    value: v_bat_raw_hidden * 3.3 / 0.2174 / 4096
    doc: Battery voltage.
  fgdos_1_sensor_frequency:
    value: fgdos1_sensor_frequency_23_16_hidden * 65536 + fgdos1_sensor_frequency_15_8_hidden * 256 + fgdos1_sensor_frequency_7_0_hidden
    doc: The current frequency of the FGDOS1 sensor.
  fgdos_1_target_frequency:
    value: fgdos1_target_frequency_23_16_hidden * 65536 + fgdos1_target_frequency_15_8_hidden * 256 + fgdos1_target_frequency_7_0_hidden
    doc: The target frequency of the FGDOS1 sensor.
  fgdos_1_recharge_frequency:
    value: fgdos1_recharge_frequency_23_16_hidden * 65536 + fgdos1_recharge_frequency_15_8_hidden * 256 + fgdos1_recharge_frequency_7_0_hidden
    doc: The recharge frequency of the FGDOS1 sensor.
  fgdos_2_sensor_frequency:
    value: fgdos2_sensor_frequency_23_16_hidden * 65536 + fgdos2_sensor_frequency_15_8_hidden * 256 + fgdos2_sensor_frequency_7_0_hidden
    doc: The current frequency of the FGDOS2 sensor.
  fgdos_2_target_frequency:
    value: fgdos2_target_frequency_23_16_hidden * 65536 + fgdos2_target_frequency_15_8_hidden * 256 + fgdos2_target_frequency_7_0_hidden
    doc: The target frequency of the FGDOS2 sensor.
  fgdos_2_recharge_frequency:
    value: fgdos2_recharge_frequency_23_16_hidden * 65536 + fgdos2_recharge_frequency_15_8_hidden * 256 + fgdos2_recharge_frequency_7_0_hidden
    doc: The recharge frequency of the FGDOS2 sensor.
