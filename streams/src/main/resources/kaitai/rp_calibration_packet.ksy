meta:
  id: rp_calibration_packet
  endian: be
  title: RP Calibration Packet

seq:
  - id: dev_id
    type: u1
  - id: package_num
    type: u2
  - id: alarm
    type: u1
  - id: alarm_counts
    type: u4
  - id: counts
    type: u4
  - id: counts1h_ago
    type: u4
  - id: counts2h_ago
    type: u4
  - id: checking_time
    type: u2
  - id: checking_time1h_ago
    type: u2
  - id: checking_time2h_ago
    type: u2
  - id: shock_counts
    type: u4
  - id: temperature
    type: u2
  - id: unused_hidden
    type: u1
  - id: mon3v3
    type: u2
  - id: mon5
    type: u2
  - id: mon_vin
    type: u2
  - id: exwdtc
    type: u2

instances:
  mon3v3_voltage:
    value: mon3v3 * 0.0008056640625
  mon5_voltage:
    value: mon5 * 0.0008056640625
  mon_vin_voltage:
    value: mon_vin * 0.00185302734375
