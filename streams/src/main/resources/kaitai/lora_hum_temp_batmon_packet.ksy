meta:
  id: lora_hum_temp_batmon_packet
  endian: le
  bit-endian: le
  title: BE Batmon's Hum Temp packet

seq:
  - id: packet_number_low_hidden
    type: u1
  - id: packet_number_high_hidden
    type: u1
  - id: swbuild
    type: u1
  - id: dummy_byte
    type: u1
  - id: status_flag_low_hidden
    type: u1
  - id: status_flag_high_hidden
    type: u1
  - id: mon_3_3_low_hidden
    type: u1
  - id: mon_3_3_high_hidden
    type: u1
  - id: mon_5_low_hidden
    type: u1
  - id: mon_5_high_hidden
    type: u1
  - id: v_bat_low_hidden
    type: u1
  - id: v_bat_high_hidden
    type: u1
  - id: extwtd_cnt_low_hidden
    type: u1
  - id: extwtd_cnt_high_hidden
    type: u1
  - id: humi_sum_period_raw_hidden
    type: u4
  - id: humi_cap0_low_hidden
    type: u1
  - id: humi_cap0_high_hidden
    type: u1
  - id: temp_gain_low_hidden
    type: u1
  - id: temp_gain_high_hidden
    type: u1
  - id: temp_raw_value_low_hidden
    type: u1
  - id: temp_raw_value_high_hidden
    type: u1

instances:
  packet_number:
    value: packet_number_low_hidden + packet_number_high_hidden * 256
  status_flag:
    value: status_flag_low_hidden + status_flag_high_hidden * 256
  mon_3_3:
    value: (mon_3_3_low_hidden + mon_3_3_high_hidden * 256) * 6.6 / 4096
  mon_5:
    value: (mon_5_low_hidden + mon_5_high_hidden * 256) * 6.6 / 4096
  v_bat:
    value: (v_bat_low_hidden + v_bat_high_hidden * 256) * 3.3 / (0.2174 * 4096)
  extwtd_cnt:
    value: extwtd_cnt_low_hidden + extwtd_cnt_high_hidden * 256
  humi_cap0:
    value: humi_cap0_low_hidden + humi_cap0_high_hidden * 256
  humi_sum_period:
    value: (humi_sum_period_raw_hidden >> 24) * 16777216 + ((humi_sum_period_raw_hidden >> 16) & 0xFF) * 65536 + ((humi_sum_period_raw_hidden >> 8) & 0xFF) * 256 + (humi_sum_period_raw_hidden & 0xFF)
  temp_gain:
    value: temp_gain_low_hidden + temp_gain_high_hidden * 256
  temp_raw_value:
    value: temp_raw_value_low_hidden + temp_raw_value_high_hidden * 256
  temperature_adc_conv:
    value: temp_raw_value * 3.3 / 4096
  temperature_gain_adc_conv:
    value: temp_gain / 0.029383982396769513
  temp_gain_is_zero_hidden:
    value: temp_gain == 0
  temperature_bridge_voltage:
    value: 'temp_gain_is_zero_hidden ? 0.0 : (temp_raw_value * 1.0) / temp_gain'
