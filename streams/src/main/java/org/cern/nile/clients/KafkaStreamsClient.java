package org.cern.nile.clients;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;
import org.apache.kafka.common.config.SaslConfigs;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.errors.DefaultProductionExceptionHandler;
import org.apache.kafka.streams.errors.LogAndContinueExceptionHandler;
import org.cern.nile.configs.Configure;
import org.cern.nile.configs.StreamConfig;
import org.cern.nile.exceptions.ReverseDnsLookupException;
import org.cern.nile.serdes.json.JsonSerde;

/**
 * This class is responsible for creating and configuring KafkaStreams instances.
 */
public class KafkaStreamsClient implements Configure {

  private Properties properties;

  @Override
  public void configure(Properties configs) {
    final String clientId = configs.getProperty(StreamConfig.ClientProperties.CLIENT_ID.getValue());
    properties = new Properties();
    properties.put(StreamsConfig.APPLICATION_ID_CONFIG, clientId);
    properties.put(StreamsConfig.CLIENT_ID_CONFIG, clientId);

    String kafkaCluster = configs.getProperty(StreamConfig.ClientProperties.KAFKA_CLUSTER.getValue());

    if (!kafkaCluster.equals("test")) {
      properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, this.reverseDnsLookup(kafkaCluster));
      properties.put(StreamsConfig.SECURITY_PROTOCOL_CONFIG, "SASL_SSL");
      properties.put(SaslConfigs.SASL_MECHANISM, "GSSAPI");
      properties.put(SaslConfigs.SASL_KERBEROS_SERVICE_NAME, "kafka");
      properties.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, configs.getProperty(StreamConfig.ClientProperties.TRUSTSTORE_LOCATION.getValue()));
    } else {
      properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, configs.getProperty("bootstrap.servers"));
    }

    properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
    properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, JsonSerde.class.getName());
    properties.put(StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG, LogAndContinueExceptionHandler.class.getName());
    properties.put(StreamsConfig.DEFAULT_PRODUCTION_EXCEPTION_HANDLER_CLASS_CONFIG, DefaultProductionExceptionHandler.class.getName());
  }

  /**
   * Creates a KafkaStreams instance using the provided topology.
   *
   * @param topology the topology to be used for the KafkaStreams instance
   * @return a configured KafkaStreams instance
   */
  public KafkaStreams create(Topology topology) {
    return new KafkaStreams(topology, properties);
  }

  /**
   * Resolves the provided Kafka cluster domain to a comma-separated list of
   * hostnames with port 9093.
   *
   * @param kafkaCluster the domain of the Kafka cluster
   * @return a comma-separated list of hostnames with port 9093
   * @throws RuntimeException if the hostname resolution fails
   */
  private String reverseDnsLookup(String kafkaCluster) {
    try {
      StringBuilder sb = new StringBuilder();
      InetAddress[] address = InetAddress.getAllByName(kafkaCluster);
      for (InetAddress host : address) {
        final String hostName = InetAddress.getByName(host.getHostAddress()).getHostName();
        // FIXME: add configuration for the port
        sb.append(hostName).append(":9093,");
      }
      sb.deleteCharAt(sb.length() - 1);
      return sb.toString();
    } catch (UnknownHostException e) {
      throw new ReverseDnsLookupException("Failed to perform reverse DNS lookup for the Kafka cluster: " + kafkaCluster, e);
    }
  }
}
