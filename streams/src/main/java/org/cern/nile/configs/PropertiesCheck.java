package org.cern.nile.configs;

import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import javax.validation.constraints.NotNull;
import org.cern.nile.exceptions.MissingPropertyException;

public final class PropertiesCheck {

  private PropertiesCheck() {
  }

  private static final Set<String> CLIENT_PROPERTIES = StreamConfig.ClientProperties.getValues();
  private static final Set<String> COMMON_PROPERTIES = StreamConfig.CommonProperties.getValues();
  private static final Set<String> OTLP_DECODING_PROPERTIES = StreamConfig.DecodingProperties.getValues();

  /**
   * Validates the properties file.
   *
   * @param properties the properties file
   */
  public static void validateProperties(@NotNull Properties properties) {
    Objects.requireNonNull(properties, "Properties object cannot be null");

    validateRequiredProperties(properties, CLIENT_PROPERTIES);
    validateRequiredProperties(properties, COMMON_PROPERTIES);
    validateRequiredProperties(properties, OTLP_DECODING_PROPERTIES);
  }

  private static void validateRequiredProperties(@NotNull Properties props, @NotNull Set<String> propsToCheck) {
    Objects.requireNonNull(props, "Properties object cannot be null");

    for (String prop : propsToCheck) {
      if (!props.containsKey(prop)) {
        throw new MissingPropertyException(String.format("Properties file is missing: %s property.", prop));
      }
    }
  }
}
