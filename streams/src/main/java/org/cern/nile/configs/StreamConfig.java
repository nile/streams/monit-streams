package org.cern.nile.configs;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A class containing enums representing various stream configuration property categories.
 */
public class StreamConfig {

  public enum ClientProperties {
    SOURCE_TOPIC("source.topic"),
    KAFKA_CLUSTER("kafka.cluster"),
    CLIENT_ID("client.id"),
    TRUSTSTORE_LOCATION("truststore.location");

    private final String value;

    ClientProperties(String value) {
      this.value = value;
    }

    public static Set<String> getValues() {
      return Arrays.stream(values()).map(o -> o.value).collect(Collectors.toSet());
    }

    public String getValue() {
      return value;
    }
  }

  public enum CommonProperties {
    STREAM_CLASS("stream.class");

    private final String value;

    CommonProperties(String value) {
      this.value = value;
    }

    public static Set<String> getValues() {
      return Arrays.stream(values()).map(o -> o.value).collect(Collectors.toSet());
    }

    public String getValue() {
      return value;
    }
  }


  public enum DecodingProperties {
    SINK_TOPIC("sink.topic");

    private final String value;

    DecodingProperties(String value) {
      this.value = value;
    }

    public static Set<String> getValues() {
      return Arrays.stream(values()).map(o -> o.value).collect(Collectors.toSet());
    }

    public String getValue() {
      return value;
    }
  }


}
