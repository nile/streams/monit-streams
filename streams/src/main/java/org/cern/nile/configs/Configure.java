package org.cern.nile.configs;

import java.util.Properties;

/**
 * Interface for classes that can be configured with a Properties object.
 */
public interface Configure {
  void configure(Properties configs);
}
