package org.cern.nile.serdes.otlp;

import io.opentelemetry.proto.logs.v1.LogsData;
import java.util.Map;
import org.apache.kafka.common.serialization.Serializer;

public class OtlpLogsSerializer implements Serializer<LogsData> {


  /**
   * Default constructor needed by Kafka.
   */
  public OtlpLogsSerializer() {
  }

  @Override
  public void configure(Map<String, ?> configs, boolean isKey) {
  }

  /**
   * Serialize a LogsData object into a byte array.
   *
   * @param topic The topic associated with the data
   * @param data  The LogsData object to be serialized
   * @return The serialized byte array or null if the LogsData object is null
   */
  @Override
  public byte[] serialize(String topic, LogsData data) {
    return data != null ? data.toByteArray() : null;
  }

  @Override
  public void close() {
  }

}
