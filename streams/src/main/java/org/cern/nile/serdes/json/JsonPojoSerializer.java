package org.cern.nile.serdes.json;

import com.google.gson.Gson;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import org.apache.kafka.common.serialization.Serializer;

public class JsonPojoSerializer<T> implements Serializer<T> {

  private static final Gson gson = new Gson();

  /**
   * Default constructor needed by Kafka.
   */
  public JsonPojoSerializer() {
  }

  @Override
  public void configure(Map<String, ?> props, boolean isKey) {
  }

  /**
   * Serialize the provided data as a JSON string and convert it to bytes.
   *
   * @param topic The topic associated with the data.
   * @param data  The data to be serialized.
   * @return The serialized data as bytes or null if the data is null.
   */
  @Override
  public byte[] serialize(String topic, T data) {
    if (data == null) {
      return null;
    }
    return gson.toJson(data).getBytes(StandardCharsets.UTF_8);
  }

  @Override
  public void close() {
  }

}
