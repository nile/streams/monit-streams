package org.cern.nile.serdes.otlp;

import com.google.protobuf.InvalidProtocolBufferException;
import io.opentelemetry.proto.logs.v1.LogsData;
import java.util.Map;
import org.apache.kafka.common.serialization.Deserializer;
import org.cern.nile.exceptions.OtlpProcessingException;

public class OtlpLogsDeserializer implements Deserializer<LogsData> {


  /**
   * Default constructor needed by Kafka.
   */
  public OtlpLogsDeserializer() {
  }

  @Override
  public void configure(Map<String, ?> configs, boolean isKey) {
  }

  /**
   * Deserialize a record value from a byte array into a LogsData object.
   *
   * @param topic The topic associated with the data
   * @param bytes The byte array to be deserialized
   * @return The deserialized LogsData object or null if the byte array is null
   */
  @Override
  public LogsData deserialize(String topic, byte[] bytes) {
    try {
      return bytes != null ? LogsData.parseFrom(bytes) : null;
    } catch (InvalidProtocolBufferException e) {
      throw new OtlpProcessingException("Failed to parse the protobuf message", e);
    }
  }

  @Override
  public void close() {
  }

}
