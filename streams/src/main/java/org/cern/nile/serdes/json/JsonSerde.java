package org.cern.nile.serdes.json;

import com.google.gson.JsonObject;
import java.util.Map;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

public class JsonSerde implements Serde<JsonObject> {
  final JsonPojoSerializer<JsonObject> serializer = new JsonPojoSerializer<>();
  final JsonPojoDeserializer<JsonObject> deserializer = new JsonPojoDeserializer<>(JsonObject.class);

  @Override
  public void configure(Map<String, ?> configs, boolean isKey) {
    serializer.configure(configs, isKey);
    deserializer.configure(configs, isKey);
  }

  @Override
  public void close() {
    serializer.close();
    deserializer.close();
  }

  @Override
  public Serializer<JsonObject> serializer() {
    return serializer;
  }

  @Override
  public Deserializer<JsonObject> deserializer() {
    return deserializer;
  }
}
