package org.cern.nile.serdes.json;

import com.google.gson.Gson;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import org.apache.kafka.common.serialization.Deserializer;

public class JsonPojoDeserializer<T> implements Deserializer<T> {

  private static final Gson gson = new Gson();
  Class<T> tClass;

  /**
   * Default constructor needed by Kafka.
   */
  public JsonPojoDeserializer() {
  }

  JsonPojoDeserializer(Class<T> clazz) {
    this.tClass = clazz;
  }

  @Override
  @SuppressWarnings("unchecked")
  public void configure(Map<String, ?> props, boolean isKey) {
    if (tClass == null) {
      tClass = (Class<T>) props.get("JsonPOJOClass");
    }
  }

  /**
   * Deserialize the provided byte array into an object of type T.
   *
   * @param topic The topic associated with the data.
   * @param bytes The byte array to be deserialized.
   * @return The deserialized object of type T or null if the byte array is null.
   */
  @Override
  public T deserialize(String topic, byte[] bytes) {
    if (bytes == null) {
      return null;
    }
    return gson.fromJson(new String(bytes, StandardCharsets.UTF_8), tClass);
  }

  @Override
  public void close() {
  }

}