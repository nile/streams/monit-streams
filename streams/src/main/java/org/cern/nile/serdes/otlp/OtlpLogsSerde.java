package org.cern.nile.serdes.otlp;

import io.opentelemetry.proto.logs.v1.LogsData;
import java.util.Map;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

public class OtlpLogsSerde implements Serde<LogsData> {

  private final Serializer<LogsData> serializer = new OtlpLogsSerializer();
  private final Deserializer<LogsData> deserializer = new OtlpLogsDeserializer();

  @Override
  public void configure(Map<String, ?> configs, boolean isKey) {
    serializer.configure(configs, isKey);
    deserializer.configure(configs, isKey);
  }

  @Override
  public void close() {
    serializer.close();
    deserializer.close();
  }

  @Override
  public Serializer<LogsData> serializer() {
    return serializer;
  }

  @Override
  public Deserializer<LogsData> deserializer() {
    return deserializer;
  }
}
