package org.cern.nile.exceptions;

public class ReverseDnsLookupException extends RuntimeException {

  public ReverseDnsLookupException(String message, Throwable cause) {
    super(message, cause);
  }

}