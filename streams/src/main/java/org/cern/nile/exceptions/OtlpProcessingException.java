package org.cern.nile.exceptions;

public class OtlpProcessingException extends RuntimeException {
  public OtlpProcessingException(String message, Throwable cause) {
    super(message, cause);
  }
}
