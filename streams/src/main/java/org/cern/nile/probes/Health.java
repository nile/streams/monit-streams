package org.cern.nile.probes;

import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Map;

import org.apache.kafka.streams.KafkaStreams;

public class Health {

  // https://javadoc.io/doc/org.apache.kafka/kafka-streams/3.4.0/index.html
  // 425 ->  Too early
  private static final Map<String,Integer>  returnCodes = Map.of(
      "CREATED",201,
      "ERROR",404,
      "NOT_RUNNING",500,
      "PENDING_ERROR",425,
      "PENDING_SHUTDOWN",425,
      "REBALANCING",503,
      "RUNNING",200);

  private static final int PORT = 8899;

  private final KafkaStreams streams;
  private HttpServer server;

  public Health(KafkaStreams streams) {
    this.streams = streams;
  }

  /**
   * Start the Health http server.
   */
  public void start() {
    try {
      server = HttpServer.create(new InetSocketAddress(PORT), 0);
    } catch (IOException ioe) {
      throw new RuntimeException("Could not setup http server: ", ioe);
    }
    server.createContext("/health", exchange -> {
      int responseCode = returnCodes.get(streams.state().name());
      exchange.sendResponseHeaders(responseCode, 0);
      exchange.close();
    });
    server.start();
  }

  /**
   * Stops the Health HTTP server.
   */
  public void stop() {
    server.stop(0);
  }

}
