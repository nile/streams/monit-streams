package org.cern.nile.streams;

import org.cern.nile.clients.KafkaStreamsClient;
import org.cern.nile.configs.Configure;

public interface Streaming extends Configure {

  void stream(KafkaStreamsClient kafkaStreamsClient);

}
