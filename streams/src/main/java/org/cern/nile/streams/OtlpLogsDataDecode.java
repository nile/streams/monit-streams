package org.cern.nile.streams;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import io.opentelemetry.proto.logs.v1.LogRecord;
import io.opentelemetry.proto.logs.v1.LogsData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Produced;
import org.cern.nile.exceptions.OtlpProcessingException;
import org.cern.nile.serdes.json.JsonSerde;
import org.cern.nile.serdes.otlp.OtlpLogsSerde;

public class OtlpLogsDataDecode extends AbstractStream {

  private static final String BODY = "body";
  private static final String KVLIST_VALUE = "kvlistValue";
  private static final String VALUES = "values";
  private static final String KEY = "key";
  private static final String VALUE = "value";
  private static final String STRING_VALUE = "stringValue";

  private final Gson gson = new Gson();
  private final JsonFormat.Printer printer = JsonFormat.printer();

  public OtlpLogsDataDecode() {
    super();
  }

  protected OtlpLogsDataDecode(String sourceTopic, String sinkTopic) {
    super(sourceTopic, sinkTopic);
  }

  @Override
  protected void createTopology(StreamsBuilder builder) {
    builder
        .stream(sourceTopic, Consumed.with(Serdes.String(), new OtlpLogsSerde()))
        .flatMap((key, value) -> {
          List<KeyValue<String, JsonObject>> results = new ArrayList<>();
          for (Map<String, Object> item : processProto(value)) {
            JsonObject jsonObject = gson.toJsonTree(item).getAsJsonObject();
            results.add(KeyValue.pair(key, jsonObject));
          }
          return results;
        })
        .to(sinkTopic, Produced.with(Serdes.String(), new JsonSerde()));
  }

  private Iterable<Map<String, Object>> processProto(LogsData logsData) {
    return logsData.getResourceLogsList().stream()
        .flatMap(resourceLog -> resourceLog.getScopeLogsList().stream())
        .flatMap(scopeLog -> scopeLog.getLogRecordsList().stream())
        .map(this::toMap)
        .collect(Collectors.toList());
  }

  @SuppressWarnings("unchecked")
  private Map<String, Object> toMap(LogRecord logRecord) {
    String jsonStr;
    try {
      jsonStr = printer.print(logRecord);
    } catch (InvalidProtocolBufferException e) {
      LOGGER.error("Failed to convert protobuf message to JSON", e);
      throw new OtlpProcessingException("Failed to convert protobuf message to JSON", e);
    }

    Map<String, Object> map = gson.fromJson(jsonStr, Map.class);
    if (isEmptyOrNull(map, BODY)) return new HashMap<>();

    Map<String, Object> kvListMap = (Map<String, Object>) map.get(BODY);
    if (isEmptyOrNull(kvListMap, KVLIST_VALUE)) return new HashMap<>();

    Map<String, Object> valuesContainer = (Map<String, Object>) kvListMap.get(KVLIST_VALUE);
    if (isEmptyOrNull(valuesContainer, VALUES)) return new HashMap<>();

    List<Map<String, Object>> valuesList = (List<Map<String, Object>>) valuesContainer.get(VALUES);

    Map<String, Object> flattenedMap = flattenValues(valuesList);
    flattenedMap.put("timestamp_unix_nano", logRecord.getTimeUnixNano());

    return flattenedMap;
  }

  private boolean isEmptyOrNull(Map<String, Object> map, String key) {
    return map == null || !map.containsKey(key);
  }

  @SuppressWarnings("unchecked")
  private Map<String, Object> flattenValues(List<Map<String, Object>> valuesList) {
    Map<String, Object> flattenedMap = new HashMap<>();
    for (Map<String, Object> entry : valuesList) {
      String key = (String) entry.get(KEY);
      Map<String, Object> valueMap = (Map<String, Object>) entry.get(VALUE);
      flattenedMap.put(key, valueMap.get(STRING_VALUE));
    }
    return flattenedMap;
  }

}
