package org.cern.nile.streams;

import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.cern.nile.clients.KafkaStreamsClient;
import org.cern.nile.configs.StreamConfig;
import org.cern.nile.probes.Health;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractStream implements Streaming {

  protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

  Properties configs;
  KafkaStreams streams;
  protected String sourceTopic;
  protected String sinkTopic;
  private Health health;
  private CountDownLatch latch;

  public AbstractStream() {
  }

  protected AbstractStream(String sourceTopic, String sinkTopic) {
    this.sourceTopic = sourceTopic;
    this.sinkTopic = sinkTopic;
  }

  @Override
  public void configure(Properties configs) {
    this.configs = configs;
  }

  @Override
  public void stream(KafkaStreamsClient kafkaStreamsClient) {
    init(kafkaStreamsClient);
    Runtime.getRuntime().addShutdownHook(new Thread(this::shutDown, "monit-streams-shutdown-hook"));
    start();
    System.exit(0);
  }

  protected abstract void createTopology(StreamsBuilder builder);

  private void init(KafkaStreamsClient kafkaStreamsClient) {
    final StreamsBuilder builder = new StreamsBuilder();
    sourceTopic = configs.getProperty(StreamConfig.ClientProperties.SOURCE_TOPIC.getValue());
    sinkTopic = configs.getProperty(StreamConfig.DecodingProperties.SINK_TOPIC.getValue());
    createTopology(builder);
    final Topology topology = builder.build();
    streams = kafkaStreamsClient.create(topology);
    health = new Health(streams);
    latch = new CountDownLatch(1);
  }

  private void start() {
    LOGGER.info("Starting monit-streams...");
    try {
      streams.start();
      health.start();
      latch.await();
    } catch (Exception e) {
      LOGGER.error("Could not start monit-streams.", e);
      System.exit(1);
    }
  }

  private void shutDown() {
    LOGGER.info("Shutting down monit-streams...");
    streams.close();
    health.stop();
    latch.countDown();
  }

}
