package org.cern.nile;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;
import org.cern.nile.clients.KafkaStreamsClient;
import org.cern.nile.configs.PropertiesCheck;
import org.cern.nile.configs.StreamConfig;
import org.cern.nile.streams.Streaming;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

  private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

  /**
   * Main method.
   *
   * @param args the properties files
   */
  public static void main(String[] args) {

    if (args.length < 1) {
      LOGGER.error("Expecting args[0] to be the path to the configuration file");
      throw new RuntimeException("Expecting args[0] to be the path to the configuration file");
    }

    // Loading properties file
    String configsPath = args[0];
    final Properties configs = new Properties();
    try {
      configs.load(new FileInputStream(configsPath));
    } catch (IOException e) {
      LOGGER.error("Error loading properties file: {}", configsPath, e);
      throw new RuntimeException(e);
    }

    PropertiesCheck.validateProperties(configs);

    final KafkaStreamsClient client = new KafkaStreamsClient();
    client.configure(configs);

    try {
      Class<?> clazz = Class.forName(configs.getProperty(StreamConfig.CommonProperties.STREAM_CLASS.getValue()));
      final Streaming streaming;
      streaming = (Streaming) clazz.getDeclaredConstructor().newInstance();
      streaming.configure(configs);
      streaming.stream(client);
    } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | ClassCastException
             | InvocationTargetException | NoSuchMethodException e) {
      LOGGER.error("Error loading stream class", e);
    }
  }
}
