# Nile Kafka Streams

Nile Kafka Streams is a framework designed for deploying Kafka Streams applications within our ecosystem. Its
primary objective is to streamline the deployment process, enabling users to easily integrate their custom decoders. In
doing so, the framework ensures that applications are efficiently launched and managed across our cluster
infrastructure.

## How it works

Nile Kafka Streams is an evolving framework designed to facilitate the deployment of routing and decoding stream
applications in our environment. Our goal is to make the process as user-friendly as possible, allowing users to easily
contribute their own decoders and have the application seamlessly integrated into our clusters.

### Main components

The framework is built around the following main components:

1. **AbstractStream**: This abstract class is the base for implementing Kafka Streams processing logic.
2. **LoraDecoderStream**: This abstract class extends `AbstractStream` and serves as the base for implementing Kafka
   Streams processing logic with Kaitai Structs. You can extend this class and customize it for your specific use case.
3. **KaitaiPacketDecoder**: This component is responsible for decoding binary data using Kaitai Structs, enabling
   efficient and user-friendly data processing within the framework.

### Kaitai

A [Kaitai Struct](https://kaitai.io/) definition (.ksy file) describes a binary data structure we wish to decode. This
definition is
then be compiled into a Java class, which is then utilized by the  **KaitaiPacketDecoder** to parse incoming binary
data, converting it into more accessible and meaningful data structures.

#### Kaitai main components

1. **Kaitai Structs**: Binary data formats are described using Kaitai Struct language. You can create `.ksy` files to
   define the structure of your binary data.
2. **Kaitai Struct Compiler**: This compiler translates the `.ksy` files into Java classes that can be used to parse
   and decode the binary data. You'll need to add the generated classes to your project.

### Usage for New Streaming Applications with Kaitai

1. **Define the binary data format with Kaitai Struct**: Create a `.ksy` file describing the structure of the binary
   data. Examples and documentation can be found on the [Kaitai Struct website](https://kaitai.io/).
2. **Compile the Kaitai Struct file**: Use the Kaitai Struct Compiler to generate the Java classes for the binary data
   format.
3. **Implement a custom decoder**: Create a class that extends `LoraDecoderStream` and provides the Kaitai Struct class
   for decoding the binary data. If needed, customize the class for the specific use case by overriding methods.
4. **Write tests**: When creating a new Kafka Streams application with Kaitai Structs, it is essential to write tests to
   ensure the correct processing and decoding of binary data.This helps confirm the functionality of the implementation
   and identify any issues early on.

### Further Configuration

You will need a configuration file `streams.properties` in which you specify the classes to use and configuration
properties described below:

#### Common Properties (ALWAYS REQUIRED):

- `stream.class`: The full class name the application will use to process the messages

#### Client Properties (REQUIRED ALWAYS):

- `client.id`: The client ID of the streams application seen by Kafka brokers
- `source.topic`: Kafka topic to source from
- `kafka.cluster`: Kafka cluster to be used
- `truststore.location`: SSL truststore location to be used for Kafka authentication

#### Decoding Properties (REQUIRED WHEN DECODING):

- `sink.topic`: Kafka topic to sink to

#### Routing Properties (REQUIRED WHEN ROUTING):

- `routing.config.path`: The path to the JSON containing the routing config

This file will have to be passed as an argument at the application starting point (
e.g., `java -jar application.jar <path to streams.properties>`).

Additionally, you will need to pass JVM properties for the Kerberos configuration:

- `-Djava.security.krb5.conf=<insert path>`: Path to the Kerberos configuration
- `-Djava.security.auth.login.config=<insert path>`: Path to the JAAS configuration file